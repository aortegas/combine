import SwiftUI
import Combine
import PlaygroundSupport

let dateFormat: DateFormatter = {

    let dateFormat = DateFormatter()
    dateFormat.locale = Locale(identifier: "es_ES")
    dateFormat.dateStyle = .none
    dateFormat.timeStyle = .medium
    return dateFormat
}()

final class ClockVM: ObservableObject {                         // ObservableObject is a publisher.

    @Published var time = ""                                    // When this property changes, the publisher send an event.
                                                                // When time change, send event and the view catch the event with the ObservedObject and redraw the view.
    var subscribers = Set<AnyCancellable>()

    init() {

        Timer.TimerPublisher(interval: 1.0, runLoop: .main, mode: .default)
            .autoconnect()
            .map { dateFormat.string(from: $0) }
            .assign(to: \.time, on: self)                       // In this example, we can use assing, due we have a class instead of a struct.
            .store(in: &subscribers)                            // We are a subcriber form TimePublisher.
    }
}

// Example 1 (receive events from publisher linked automatically):
// -----------------------------------------------------------------
// Probably the easiest method  to work with Swift UI.
struct ClockView1: View {

    @ObservedObject var clockVM = ClockVM()                     // With Property wrapper that subscribes to an ObservableObject and invalidates a view (redraw the view) whenever the ObservableObject changes.
                                                                // We don't need onReceive to subcribe to publisher.

    var body: some View {

        VStack {
            Text("Example 1: From publisher linked automatically:")
                .font(.headline)
            Text("\(clockVM.time)")                             // Important: In this moment view and viewModel are linked automatically without use onReceive.
                .frame(maxWidth: .infinity)
        }
    }
}

PlaygroundPage.current.setLiveView(ClockView1())


// Example 2 (receive events from observed property linked manually):
// --------------------------------------------------------------------
// In the example 1, we work with ObservedObject that it was linked automatically with @Published time variable and receive the events change.
// In this example, we link manually with the @Published time variable using onReceive and state var.
//struct ClockView2: View {
//
//    @ObservedObject var clockVM = ClockVM()
//
//    @State var timePublished = ""                               // For this example, we need to retain time property states in the view.
//
//    var body: some View {
//
//        VStack {
//            Text("Example 2: From observed property linked manually:")
//                .font(.headline)
//            Text("\(timePublished)")                            // In this example, we show the timePublished variable, instead of vm (clockVM.time) variable.
//                .frame(maxWidth: .infinity)
//                .onReceive(clockVM.$time) { time in             // We subscribe to property: (clockVM.time) and link manually with the viewModel.
//                    timePublished = time                        // Important: $ indicator is the wrapper value from @Published, and this is a time variable (string).
//                }
//        }
//    }
//}
//
//PlaygroundPage.current.setLiveView(ClockView2())


// Example 3 (example 2 & receive events from from publisher linked manually):
// ---------
// ObservableObject send event changes before the changes have occurred (willChange).
// In this example, we learn to link manually with the @Published time variable and ObservableObject with onReceive (is a combination between example 1 and example 2).
// Important: See the difference between the two timers.
//struct ClockView3: View {
//
//    @ObservedObject var clockVM = ClockVM()
//
//    @State var timePublished = ""                                   // Is the same as example 2.
//    @State var timeObservableObject = ""                            // Example 3.
//
//    var body: some View {
//
//        VStack {
//            Text("Example 3: From observed property linked manually:")
//                .font(.headline)
//            Text("\(timePublished)")
//                .frame(maxWidth: .infinity)
//                .onReceive(clockVM.$time) { time in
//                    timePublished = time
//                }
//
//            Text("Example 3: From publisher linked manually:")
//                .font(.headline)
//            Text("\(timeObservableObject)")
//                .frame(maxWidth: .infinity)
//                .onReceive(clockVM.objectWillChange) {              // The difference with example 2, here get objectWillChange that receives event before changes the observed property.
//                    timeObservableObject = clockVM.time             // So we need to get time from the variable time directly.
//                }
//        }
//    }
//}
//
//PlaygroundPage.current.setLiveView(ClockView3())

