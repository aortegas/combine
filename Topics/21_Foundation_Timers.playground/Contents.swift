import SwiftUI
import Combine
import PlaygroundSupport

let dateFormat: DateFormatter = {
    
    let dateFormat = DateFormatter()
    dateFormat.locale = Locale(identifier: "es_ES")
    dateFormat.dateStyle = .none                                        // We only show the time in our clock.
    dateFormat.timeStyle = .medium
    return dateFormat
}()

struct MyClock: View {
    
    let timer = Timer                                                   // We create the Timer (publisher in this example).
        .TimerPublisher(interval: 1, runLoop: .main, mode: .default)    // We get the time publisher on main thread, due we update the UI. We get an event with every second.
        .autoconnect()                                                  // The timers are a special publishers. We need to run publisher because the timer is the source to emits events.
                                                                        // Other publishers are listening changes of his source from the begining of his creation.
                                                                        // For this reason we need to "connect" the publisher with the connect operator.
                                                                        // If we use the connect operator we must also use the disconnect operator, but with the autoconnect operator we don't need disconect operator.
        .map { dateFormat.string(from: $0) }                            // $0 is a current Date for each event.
                                                                        // Important: Another option will be to do assign, but in this case, we can't do it, because we are in a struct and it's inmutable.
                                                                        // So we can use onReceiver above.

    @State var hour = ""                                                // We need a var state to manage changes of the clock.

    var body: some View {
        
        VStack {
            
            Text("\(hour)")                                             // Show the var state.
                .frame(width: 200)
                .onReceive(timer) { time in                             // Important: With onReceive function is similar to sink operator in Combine. We connect the event of the publisher with the view.
                                                                        // Get the publisher (timer) and in the clousure get the value of the event and update the text of the view.
                    hour = time
                }
        }
    }
}

PlaygroundPage.current.setLiveView(MyClock())                           // This is need, when we work with Swift UI in the Playgrounds.
