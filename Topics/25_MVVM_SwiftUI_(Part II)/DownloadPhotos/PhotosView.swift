//
//  PhotosView.swift
//

import SwiftUI

struct PhotosView: View {
    
    @ObservedObject var photosVM = PhotosVM()
    
    let columns: [GridItem] = .init(repeating: .init(.flexible()), count: 2)
    
    var body: some View {
        
        ScrollView {
            
            LazyVGrid(columns: columns) {
                
                ForEach(photosVM.photos) { photo in

                    photo.image
                        .resizable()
                        .scaledToFit()
                }
            }
        }
        .onAppear {
            photosVM.getPhotos()
        }
    }
}

struct ContentView_Previews: PreviewProvider {

    static var previews: some View {
        PhotosView()
    }
}
