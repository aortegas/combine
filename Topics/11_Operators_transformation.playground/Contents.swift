import Foundation
import Combine

func newPublisher(value: String) -> CurrentValueSubject<String, Never> {
    .init(value.uppercased())  // We must always init a CurrentValueSubject
}

let publisherOne = ["TextOne", "TextTwo", "TextThree", "TextFour", "TextFive", "TextSix", "TextSeven"].publisher

// Operator: map & flatmap
// -----------------------

let publisherTwo = publisherOne         // Transform publisherOne in another publisher: publisherTwo. Let's see that how we do it.
    .map(\.localizedLowercase)          // Next sentence is the same to: .map { $0.lowercased() }. Important: map can use KeyPath "\" and the property: .localizedLowercase of the object $0.
    .flatMap(maxPublishers: .max(3)) {
                                        // In this case: .flatmap returns an array of three publishers with type: CurrentValueSubject<String, Never>.
                                        // We store this array in another publisher called publisherTwo.
                                        // publisherTwo emit values of type: String for each publisher that it has, like a flapmap would do with arrays of arrays.
        newPublisher(value: $0)
    }
    
let subscriber = publisherTwo.sink(receiveValue: {
    print($0)
})
