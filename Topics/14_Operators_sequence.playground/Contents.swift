import Foundation
import Combine

let publisher = [5,3,7,1,3,9,2].publisher

// Operator: min
// -------------

publisher
    .min()
    .sink { value in
        print("Minimum \(value)")
    }

// Operator: max
// -------------

publisher
    .max()
    .sink { value in
        print("Maximum \(value)")
    }

// Operator: min y max with various calculations
// ---------------------------------------------

let pub2 = ["Hola", "K", "ase"].publisher

pub2
    .min { $0.count < $1.count }
    .sink { value in
        print("Word with minimum characters: \(value)")
    }

pub2
    .max { $0.count < $1.count }
    .sink { value in
        print("Word with maximum characters: \(value)")
    }

// Operator: first
// ---------------

publisher
    .first()
    .sink { value in
        print("First value: \(value)")
    }

// Operator: last
// --------------

publisher
    .last()
    .sink { value in
        print("Last value: \(value)")
    }

// Operator: output
// ----------------

publisher
    .output(at: 2)
    .sink { value in
        print("Index 2 with value: \(value)")
    }

// Operator: output with range
// ---------------------------

publisher
    .output(in: 2...4)
    .sink { value in
        print("Index from 2 to 4 with value: \(value)")
    }

// Operator: count
// ---------------

publisher
    .count()
    .sink { value in
        print("Total events broadcast: \(value)")
    }
