
![](swift.png)

# Combine

#### Customize handling of asynchronous events by combining event-processing operators.

## Topics

#### 1 - Closures

No escaping closures & escaping closures.

- [Closures Playground](Topics/1_Closures.playground/Contents.swift)


#### 2 - Result

How to use Result type.

- [Result Playground](Topics/2_Result.playground/Contents.swift)


#### 3 - Functional Programmation

Functional Programmation concepts.

- [Functional Programmation Playground](Topics/3_Functional_Programation.playground/Contents.swift)


#### 4 - Futures and Promises

Undertanding Futures and Promises.

- [Futures and Promises Playground](Topics/4_Futures_and_Promises.playground/Contents.swift)


#### 5 - Publishers

Set values with 'assign' of publisher to an custom object.

- [Publishers Playground](Topics/5_Publishers.playground/Contents.swift)


#### 6 - Basic operators

Use operators with publishers, before subscribers recibe its events.

- [Basic operators Playground](Topics/6_Basic_Operators.playground/Contents.swift)


#### 7 - Subscribers

Example a custom Publisher class.

- [Subscribers Playground](Topics/7_Subscribers.playground/Contents.swift)


#### 8 - Imperative Subjects

Create an imperative subject (publisher) with CurrentValueSubject.

- [Imperative Subjects Playground](Topics/8_ImperativeSubjects.playground/Contents.swift)


#### 9 - Declarative Subjects

Create a declarative subject (publisher) with PassthroughSubject.

- [Declarative Subjects Playground](Topics/9_DeclarativeSubjects.playground/Contents.swift)


#### 10 - Cancel subscriptions

Because all subscribers are AnyCancellable.

- [Cancel subscriptions Playground](Topics/10_Cancel_Subscriptions.playground/Contents.swift)


#### 11 - Operators: Transformation

Operators: map, flatmap.

- [Operators: map, flatmap Playground](Topics/11_Operators_transformation.playground/Contents.swift)

#### 12 - Operators: Filter

Operators: filter, removeDuplicates, ignoreOutput, first, last, drop.

- [Operators: filter, removeDuplicates, ignoreOutput, first, last, drop Playground](Topics/12_Operators_filter.playground/Contents.swift)

#### 13 - Operators: Combination.

Operators: append, prepend, merge, combineLatest, zip.

- [Operators: append, prepend, merge, combineLatest, zip Playground](Topics/13_Operators_combination.playground/Contents.swift)

#### 14 - Operators: Sequence.

Operators: min, max, first, last, output, count.

- [Operators: min, max, first, last, output, count Playground](Topics/14_Operators_sequence.playground/Contents.swift)

#### 15 - Operators: With error manage.

Operators: tryCompactMap, collect.

- [Operators: tryCompactMap, collect Playground](Topics/15_Operators_errors.playground/Contents.swift)

#### 16 - Operators: Debug.

Operators: handleEvents, breakpoint, print.

- [Operators: handleEvents, breakpoint, print Playground](Topics/16_Operators_debug.playground/Contents.swift)

#### 17 - Foundation: URLSession with Futures.

URLSession with Futures.

- [Foundation: URLSession with Futures Playground](Topics/17_Foundation_Network_Futures.playground/Contents.swift)

#### 18 - Foundation: URLSession with DataTaskPublisher.

URLSession with dataTaskPublisher.

- [Foundation: URLSession with DataTaskPublisher Playground](Topics/18_Foundation_Network_Publishers.playground/Contents.swift)

#### 19 - Foundation: URLSession with DataTaskPublisher managing errors.

URLSession with dataTaskPublisher mananging timeout and 404 erros.

- [Foundation: URLSession with DataTaskPublisher managing errors Playground](Topics/19_Foundation_Network_Errors.playground/Contents.swift)

#### 20 - Foundation: Notification Center as a publisher.

Notification Center as a publisher.

- [Foundation: URLSession with DataTaskPublisher managing errors Playground](Topics/20_Foundation_Notification_Center.playground/Contents.swift)

#### 21 - Foundation: Timer as a publisher.

We create a clock in Swift UI working with Timer as a publisher. We connect the events of the publisher with the view, with onReceive.

- [Foundation: Timer as a publisher Playground](Topics/21_Foundation_Timers.playground/Contents.swift)

#### 22 - Foundation: KVO as a publisher.

We can to have a publisher for all objects of NSObject and the properties dynamic var.

- [Foundation: KVO as a publisher Playground](Topics/22_Foundation_KVO.playground/Contents.swift)

#### 23 - Combine in MVVM & SwiftUI.

Create a custom publisher with ObservableObject (VM) and @Published property:
- Example 1: receive events from publisher linked automatically.
- Example 2: receive events from observed property linked manually with onReceive.
- Example 3: example 2 & receive events from from publisher linked manually with objectWillChange.

- [Combine in MVVM & SwiftUI Playground](Topics/23_MVVM_SwiftUI.playground/Contents.swift)

#### 24 - Publisher chaining.

Create some url request to share outputs of some with the inputs of others.
- We use .share, .eraseToAnyPublisher, .flatmap operators
- Create a publisher with operations (in this case Zip4) using Publisher.Zip4 constructor.

- [Publisher chaining Playground](Topics/24_Complete_Data_Struct.playground/Contents.swift)

#### 25 - Combine in MVVM & SwiftUI. (Part II)

Project to show MVVM & Combine, using Model, VM and View in SwiftUI.
- We use @ObservableObject & @ObservedObject
- Publishers.MergeMany

- [Combine in MVVM & SwiftUI. (Part II)](Topics/25_MVVM_SwiftUI_(Part II)/DownloadPhotos.xcodeproj)


