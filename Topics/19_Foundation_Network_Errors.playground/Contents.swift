import Foundation
import Combine

enum Department: String, Decodable, CustomStringConvertible {
    case accounting = "Accounting"
    case businessDevelopment = "Business Development"
    case engineering = "Engineering"
    case humanResources = "Human Resources"
    case legal = "Legal"
    case marketing = "Marketing"
    case productManagement = "Product Management"
    case researchAndDevelopment = "Research and Development"
    case sales = "Sales"
    case services = "Services"
    case support = "Support"
    case training = "Training"
    
    var description: String {
        return "Departament: \(self.rawValue)"
    }
}

enum Gender: String, Decodable, CustomStringConvertible {
    case female = "Female"
    case male = "Male"

    var description: String {
        return "Gender: \(self.rawValue)"
    }
}

// Model
struct Employee: Decodable, Identifiable {
    let id: Int
    let username: String
    let firstName: String
    let lastName: String
    let gender: Gender
    let email: String
    let department: Department
    let address: String
    let avatar: URL
}

// Download data for our model from these url:
let urlEmployees = URL(string: "https://acoding.academy/testData/EmpleadosData.json")!
let urlTest200_5 = URL(string: "https://httpstat.us/200?sleep=5000")!
let urlTest404 = URL(string: "https://httpstat.us/404")!

// Possible Errors
enum NetworkError: Error, LocalizedError {
    case timeout(String)
    case notFound(String)
    case badConnection(String)
    case defaultError(String, Int)

    var errorDescription: String? {

        switch self {
        case .timeout(let error):
            return "Timeout error: \(error)"
        case .notFound(let error):
            return "Url not found error: \(error)"
        case .badConnection(let error):
            return "Bad connection error: \(error)"
        case .defaultError(let error, let errorCode):
            return "Unknown error: \(error), code: \(errorCode)"
        }
    }
}

var subscribers = Set<AnyCancellable>()

// Config URLSession
let configuration = URLSessionConfiguration.ephemeral               // With ephemeral doesn't allow connection monitoring.
configuration.timeoutIntervalForRequest = 3
let session = URLSession(configuration: configuration)

// Config Codable
let jsonDecoder = JSONDecoder()
jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase


// Example 1: datataskPublisher without errors
// -------------------------------------------
session
    .dataTaskPublisher(for: urlEmployees)
    .retry(3)                                                       // After exceeding the specified number of retries, the publisher passes the failure to the downstream receiver.
//    .mapError { error -> NetworkError in
//
//        if error.errorCode == -1001 {
//            return .timeout(error.localizedDescription)
//        }
//        else {
//            return .defaultError(error.localizedDescription, error.errorCode)
//        }
//    }
    .tryMap { (data, response) -> Data in                           // We manage ok cases (data & response). In this example we don't manage the error cases.

        guard let response = response as? HTTPURLResponse else {
            throw NetworkError.badConnection("Example 1: Wrong connection")
        }

        if response.statusCode == 200 {
            return data
        }
        else {
            throw NetworkError.notFound("Example 1: Status code: \(response.statusCode)")
        }
    }
    .decode(type: [Employee].self, decoder: jsonDecoder)
    .sink { completion in

        if case .failure(let error) = completion, let networkError = error as? NetworkError {
            print("Example 1: Completion Error: \(networkError)")
        }
    }
    receiveValue: { employees in
        print("Example 1: This is the first employer: \(employees.first!) and we have: \(employees.count) employees.")
    }
    .store(in: &subscribers)


// Example 2: datataskPublisher with timeout error
// -----------------------------------------------
// In this example we use an url with a timeout.
// We are trying 3 times (retry operator) to dataTask.
// Remember: our session is set to 3 second waiting for each attempt (timeoutIntervalForRequest).
// After the first attempt, we retry 2 more times (3 seconds each [timeoutIntervalForRequest]) and finally get a timeout error.
session
    .dataTaskPublisher(for: urlTest200_5)
    .retry(3)
    .mapError { error -> NetworkError in                                  // We manage error cases: We can change network errors by own error codes (NetworkError enum).

        if error.errorCode == -1001 {
            return .timeout(error.localizedDescription)                   // We return a NetworkError.timeout error because 1001 is a error code of timeout.
        }
        else {
            return .defaultError(error.localizedDescription, error.errorCode)
        }
    }
    .tryMap { (data, response) -> Data in

        guard let response = response as? HTTPURLResponse else {
            throw NetworkError.badConnection("Example 2: Wrong connection")
        }

        if response.statusCode == 200 {
            return data
        }
        else {
            throw NetworkError.notFound("Example 2: Status code: \(response.statusCode)")
        }
    }
    .decode(type: [Employee].self, decoder: jsonDecoder)
    .sink { completion in

        if case .failure(let error) = completion, let networkError = error as? NetworkError {
            print("Example 2: Completion Error: \(networkError)")
        }
    }
    receiveValue: { employees in
        print("Example 2: This is the first employer: \(employees.first!) and we have: \(employees.count) employees.")
    }
    .store(in: &subscribers)


// Example 3: datataskPublisher with 404 error
// -------------------------------------------
// In this example we use an url with a 404 error.
session
    .dataTaskPublisher(for: urlTest404)
    .retry(3)
//    .mapError { error -> NetworkError in
//
//        if error.errorCode == -1001 {
//            return .timeout(error.localizedDescription)
//        }
//        else {
//            return .defaultError(error.localizedDescription, error.errorCode)
//        }
//    }
    .tryMap { (data, response) -> Data in

        guard let response = response as? HTTPURLResponse else {
            throw NetworkError.badConnection("Example 3: Wrong connection")
        }

        if response.statusCode == 200 {
            return data
        }
        else {
            throw NetworkError.notFound("Example 3: Status code: \(response.statusCode)")   // We return a NetworkError.timeout error because 1001 is a error code of timeout.
        }
    }
    .decode(type: [Employee].self, decoder: jsonDecoder)
    .sink { completion in

        if case .failure(let error) = completion, let networkError = error as? NetworkError {
            print("Example 3: Completion Error: \(networkError)")
        }
    }
    receiveValue: { employees in
        print("Example 3: This is the first employer: \(employees.first!) and we have: \(employees.count) employees.")
    }
    .store(in: &subscribers)
