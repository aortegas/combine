import Foundation

// Synchronous function example
let f = { (a:Int, b:Int) -> Int in
    a+b
}

f(4,5)

// Synchronous function example
// No escaping closure example
class TestOne {
    
    var x = 10
    
    func noEscapa(completion: () -> Void) {
        completion() // The closure is executed whitin the scope of the class, due that isn't not escaping
    }
    
    func test() {
        noEscapa(completion: { x = 20 })
    }
}

let testOne = TestOne()
testOne.x
testOne.test()
testOne.x

// Asynchronous function example
// Escaping closure example
var array:[() -> Void] = []

class TestTwo {
    
    var x = 10

    func escapa(completion: @escaping () -> Void) {
        array.append(completion)
    }
    
    func test() {
        
        escapa(completion: { print("Hola") })
        
        escapa(completion: { [weak self] in
            
            guard let `self` = self else { return }
            self.x = self.x + 5
            print(self.x)
        })
    }
}

do {
    
    let testTwo = TestTwo()
    testTwo.x
    testTwo.test()
    testTwo.x
    array.last?() // Returns 15 due that testTwo is retained
}

array.first?() // Returns "Hola"
array.last?() // Nothing returns due that testTwo isn't retained in do statement




