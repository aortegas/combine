
import UIKit
import Combine

// Part 1. URLSession with dataTaskPublisher
// -----------------------------------------

var subscribers = Set<AnyCancellable>()
let url = URL(string: "https://i.blogs.es/f7b0ed/steve-jobs/2560_3000.jpg")!

URLSession.shared
    .dataTaskPublisher(for: url)
    .map(\.data)                                // We get a data of the tuple (data, response) with keyPath.
    .compactMap({ UIImage(data: $0) })          // We use compactMap to remove possible nil.
    .sink { completion in

        switch completion {
        case .finished:
            print("We have finished part 1")
        case .failure(let error):
            print("Error in part 1: \(error)")
        }
    }
    receiveValue: { image in

        print("Image in part 1: \(image)")
        image
    }
    .store(in: &subscribers)

// Part 2: Improve example 1 to manage a posible error
// ---------------------------------------------------

let urlError = URL(string: "https://i.blogs.es/f7b0ed/stev-jobs/2560_3000.jpg")!

URLSession.shared
    .dataTaskPublisher(for: urlError)
    .tryMap { (data, response) -> Data in       // error is manage by Error of result of the publisher, managed in the subscriber.
                                                // We use tryMap, due we throw a exception.
        if let response = response as? HTTPURLResponse, response.statusCode == 200 {
            return data
        }
        else {
            throw URLError(.badURL)             // The code of this error is -1000
        }
    }
    .compactMap({ UIImage(data: $0) })
    .sink { completion in

        switch completion {
        case .finished:
            print("We have finished part 2")
        case .failure(let error):             // In case that an exception is thrown before.
            print("Error in part 2: \(error)")
        }
    }
    receiveValue: { image in

        print("Image in part 2: \(image)")
        image
    }
    .store(in: &subscribers)

// Part 3: Improve example 2 replacing error by an image
// -----------------------------------------------------

URLSession.shared
    .dataTaskPublisher(for: urlError)
    .tryMap { (data, response) -> Data in

        if let response = response as? HTTPURLResponse, response.statusCode == 200 {
            return data
        }
        else {
            throw URLError(.badURL)
        }
    }
    .compactMap({ UIImage(data: $0) })
    .replaceError(with: UIImage(named: "steve-jobs.jpg", in: .main, with: .none)!)                  // Use replaceError if we have an error thrown previously
    .sink(receiveValue: { image in                                                                  // We don't need completion, because we have an image ever.

        print("Image in part 3: \(image)")
        image

    })
    .store(in: &subscribers)

// Part 4: Improve example 3 including all this code whitin a class and use assing, to get image into a class
// ----------------------------------------------------------------------------------------------------------

final class SteveJobs {

    var myImage: UIImage?                       // Is optional in case of replaceError operator doesn't get a Image.
    var subscribers = Set<AnyCancellable>()
    let url = URL(string: "https://i.blogs.es/f7b0ed/steve-jobs/2560_3000.jpg")!

    func getJobs() {

        URLSession.shared
            .dataTaskPublisher(for: url)
            .tryMap { (data, response) -> Data in

                if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    return data
                } else {
                    throw URLError(.badURL)
                }
            }
            .compactMap { UIImage(data: $0) }
            .replaceError(with: UIImage(named: "steve-jobs.jpg", in: .main, with: .none))
            .assign(to: \.myImage, on: self)      // We assing the image to our image myImage in this class.
            .store(in: &subscribers)
    }
}

let jobs = SteveJobs()
jobs.getJobs()

sleep(1)    // We need to let any time to convert this example in something asyncronous to allow download the image.

print("Image in part 4: \(String(describing: jobs.myImage))")
