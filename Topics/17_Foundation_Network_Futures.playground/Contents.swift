import UIKit
import Combine

let url = URL(string: "https://i.blogs.es/f7b0ed/steve-jobs/2560_3000.jpg")!

enum NetworkError: Error {
    case defaultError
    case notFound(Int)
    case notValid
}

func getImage(url: URL) -> Future<Data, NetworkError> {    // We can't return a UIImage because all is asyncronous.
    
    .init { promise in  // Construct of Future. The Future is a publisher with one use.
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let data = data,
                  let response = response as? HTTPURLResponse,
                  error == nil else {
                
                promise(.failure(.defaultError))
                return
            }
            
            if response.statusCode == 200 {
                promise(.success(data))
            }
            else {
                promise(.failure(.notFound(response.statusCode)))
            }
        }
        
        .resume()
    }
}

var subscribers = Set<AnyCancellable>()

getImage(url: url)
    .compactMap { UIImage(data: $0) }   // If we have no errors, we transform Data into UIImage.
    .sink { completion in
        
        switch completion {
        case .finished:
            print("Successfully completed")
            
        case .failure(let error):
            
            if case .notFound(let code) = error {
                print("Image not found, code: \(code)")
            }
            else {
                print("Has been an error: \(error)")
            }
        }
    }
    receiveValue: { image in
        
        print(image)
        image
    }
    .store(in: &subscribers)      // We need to retain the subscriber. This is not neccesary on the playground.
