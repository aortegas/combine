import Foundation
import Combine

enum ErroresVarios: Error, LocalizedError {

    case errorTypeOne(Int)
    case errorTypeTwo
    case errorTypeThree
    
    var errorDescription: String? {
        
        switch self {
        case .errorTypeOne(let value):
            return "Error type one with value \(value)"
        case .errorTypeTwo:
            return "Error type two"
        case .errorTypeThree:
            return "Error type three"
        }
    }
}

// We can create our custom publishers (subjects). In this case, declarative form with PassthroughSubject.
// PassthroughSubject is a subject that broadcasts elements to downstream subscribers.
// A diference with CurrentValueSubject, a PassthroughSubject doesn't save values, in other words:
// - We don't initialize.
// - We don't have value property.
// PassthroughSubject are prefer in declarative form.
let subject = PassthroughSubject<Int, ErroresVarios>()  // No need initialize declarative subject. Including Errors in publisher for this example.

subject.send(1)

let subscriber = subject.sink { completion in
    
    switch completion {
    case .finished: print("It's ok")
    case .failure(let error): print("Something was wrong: \(error.localizedDescription)")
    }
}
receiveValue: { value in
    print("Has received value: \(value).")
}

subject.send(2)
sleep(1)            // Simulated async
subject.send(3)
sleep(1)            // Simulated async

//subject.send(completion: .finished)
subject.send(completion: .failure(.errorTypeOne(3)))

sleep(1)            // Simulated async

subject.send(4)     // We can't received this value after send finished or failure to the publisher.
