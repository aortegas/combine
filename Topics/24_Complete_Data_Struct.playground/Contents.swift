import Foundation
import SwiftUI
import Combine

// How to make many request with chaining publishers in one step.
// -------------------------------------------------------------
// Important: We don't manage Errors in this example.

var subscribers = Set<AnyCancellable>()

// Models
struct Post: Identifiable, Codable {
    
    let id: Int

    struct Rendered: Codable {
        let rendered: String
    }

    let title: Rendered
    let excerpt: Rendered
    let jetpack_featured_media_url: URL
    let author: Int
}

struct Author: Identifiable, Codable {

    let id: Int
    let name: String

    struct AvatarURLs: Codable {

        let _96: URL

        enum CodingKeys: String, CodingKey {                // To convert _96 key (in our model) to "96" (name of the key in json file)
            case _96 = "96"
        }
    }

    let avatar_urls: AvatarURLs
}

let jsonDecoder = JSONDecoder()


// See both json:
// We only need:
// - Title of post (Post.title.rendered)
// - Image of post (Post.jetpack_featured_media_url)
// - Author (Post.author -> Author.id -> Author.name)
// - Image of author (Post.author -> Author.id -> Author.avatar_urls._96)
let urlPosts = URL(string: "https://applecoding.com/wp-json/wp/v2/posts")!
let urlAuthor = URL(string: "https://applecoding.com/wp-json/wp/v2/users")!

// Important: Publishers chaining:
// postPublisher -> imagePostPublisher (step 2)
//               -> authorPublisher    (step 3) -> avatarAuthorPublisher (step 4)


// Step 1
// ------
// Get all Posts and stay with the first.
let postPublisher = URLSession.shared
    .dataTaskPublisher(for: urlPosts)
    .map(\.data)                                            // We don't manage errors in this case.
    .decode(type: [Post].self, decoder: jsonDecoder)
    .compactMap { $0.first }
    .share()                                                // To share output stream with many subscribers in one send. See imagePublisher & authorPublisher subscribers below (step 2 & 3)
    .eraseToAnyPublisher()                                  // Pack the type of publisher in other with type AnyPublisher, due the type of publisher is too long.
                                                            // We pack ...Publisher<Post, Error> into AnyPublisher<Post, Error>

// Step 2
// ------
// Get the image of the Post.
func getImagePublisher(url: URL) -> AnyPublisher<UIImage, Error> {

    URLSession.shared
        .dataTaskPublisher(for: url)
        .map(\.data)
        .compactMap { UIImage(data: $0) }
        .mapError { $0 as Error }                           // If we have an error, we transform URLError in Error type.
        .eraseToAnyPublisher()
}

let imagePostPublisher = postPublisher                      // We convert a postPublisher in an other publisher (AnyPublisher<UIImage, Error>) with flatmap.
    .flatMap { post in
        getImagePublisher(url: post.jetpack_featured_media_url)
    }


// Step 3
// ------
// Get the info of Author from the Post.
func getAuthorPublisher(author: Int) -> AnyPublisher<Author, Error> {

    let authorURL: URL = urlAuthor.appendingPathComponent("\(author)")
    
    return URLSession.shared
                .dataTaskPublisher(for: authorURL)
                .map(\.data)
                .decode(type: Author.self, decoder: jsonDecoder)
                .eraseToAnyPublisher()
}

let authorPublisher = postPublisher                         // We convert a postPublisher in an other publisher (AnyPublisher<Author, Error>) with flatmap.
    .flatMap { post in
        getAuthorPublisher(author: post.author)
    }


// Step 4
// ------
// Get the image of the Author with the getImagePublisher funtion used above (step 2).
let avatarAuthorPublisher = authorPublisher                 // We convert a authorPublisher in an other publisher (AnyPublisher<UIImage, Error>) with flatmap.
    .flatMap { author in
        getImagePublisher(url: author.avatar_urls._96)
    }


// Step 5
// ------
// With Publishers we can create a publisher with a special operation, in this case, merge four publishers with Zip4 in a one publisher, with one event.
// Until all four publishers finish and publish their events, we don't receive the event in the next publisher that we are creating:
Publishers.Zip4(postPublisher, imagePostPublisher, authorPublisher, avatarAuthorPublisher)
    .sink { completion in

        if case .failure(let error) = completion {
            print("Something was wrong: \(error)")
        }
    }
    receiveValue: { post, postImage, author, authorAvatar in
        print("Title : \(post.title.rendered)")
        print(postImage)
        postImage
        print("Author : \(author.name)")
        print(authorAvatar)
        authorAvatar
    }
    .store(in: &subscribers)

