import Foundation
import Combine

// We can create our publishers (subjects). In this case, imperative form with CurrentValueSubject.
// CurrentValueSubject is a subject that wraps a single value and publishes a new element whenever the value changes.
let subject = CurrentValueSubject<Int,Never>(0)     // Is necessary initialize with an initial value, in this case (0).
subject.value

// With send we are doing two things: save the new value and publish this value.
subject.send(1)
subject.send(2)
subject.value       // 2 Is the last value for subject.

let subscription = subject.sink(receiveValue: { print("Recibí el valor \($0).") })  // Create a subscriber.
// Subscriber receives the last value (2).

subject.send(3) // Subcriber receive the value change.

subject.send(completion: .finished) // When subject send a finished event, there are no more events to send to the subscriber.

subject.send(4) // This change doesn't reach to the subscriber.
