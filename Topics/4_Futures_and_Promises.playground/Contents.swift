import Foundation
import Combine

// Example: I can return a Int value.
func incremento(valor: Int) -> Int {
    valor + 1
}

incremento(valor: 10)

// Example: In this case, I can't return a Int Value in the function
func incrementoTiempo(valor: Int) {
    
    DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
        print(valor + 1)
    })
}

incrementoTiempo(valor: 15)

// For return something in this function, I can return a Future.
func incrementoFuturo(valor: Int, tiempo: TimeInterval) -> Future<Int, Never> {

    Future<Int, Never> { promise in

        DispatchQueue.global().asyncAfter(deadline: .now() + tiempo) {
            
            let result: Result<Int, Never> = .success(valor + 1)
            promise(result)    // execute promise (closure with a result type like a parameter)
        }
    }
    
    // To understand this:
    //
    // 1. A Future type is returned in the function, which is basically a Result type with the types that we can return, in this case: Int and Never.
    //    Never is similar to Void, it's a type without values. Never means that an error can never be returned.
    //
    // 2. The constructor of the Future type accepts a closure as an input parameter. In this closure, a (promise) is passed (which is in turn another closure of type Promise).
    //    Promise is typealias Promise = (Result<Output, Failure>) -> Void. In this case Output is of type Int.
    //
    // 3. So the constructor executes the first closure that performs our business logic (asynchronous) and when it finishes, it executes the second closure (promise),
    //    passing the result, which is of the same type as the Future. What the subscribers of the future receive, will be these types <Int, Never>.
    //    That is, it seems that the Future, when we execute the closure promise with the result of our asynchronous logic, emits events with the result, to its subscribers.
}

let future = incrementoFuturo(valor: 4, tiempo: 4) // I have a future, but what can I with it?
print("Esto es un futuro: \(future)")

let subscriber = future.sink { // Basic constructor of this subscriber.
    print("This is: \($0)")   // With sink get a subscriber from future. $0 is a Int (success type) from the future.
}

let future2 = incrementoFuturo(valor: 6, tiempo: 6)

let subscriber2 = future2.sink(receiveCompletion: { completion in // Another constructor of this subscriber, receive one event and finish event.
    
    if case .finished = completion {
        print("Ha terminado")
    }
},
receiveValue: { num in
    print("Ha llegado un numero: \(num)")
})
    
