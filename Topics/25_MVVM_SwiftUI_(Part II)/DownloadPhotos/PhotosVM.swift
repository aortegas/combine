//
//  PhotosVM.swift
//

import SwiftUI
import Combine

final class PhotosVM: ObservableObject {

    @Published var photos: [Photo] = []
        
    private var subscribers = Set<AnyCancellable>()
    
    // 1. Get an array of AnyPublisher<Photo, URLError> with images url of PhotoModel
    private var photoPublishers: [AnyPublisher<Photo, URLError>] {
        PhotosModel().imagesURL.map { getPhotoPublisher(url: $0) }                  // For each Url in the model, we get a AnyPublisher<Photo, URLError>, so we have an array of publishers.
    }
    
    private func getPhotoPublisher(url: URL) -> AnyPublisher<Photo, URLError> {

        URLSession.shared
            .dataTaskPublisher(for: url)
            .map(\.data)
            .compactMap { UIImage(data: $0) }
            .map { Photo(image: Image(uiImage: $0)) }
            .eraseToAnyPublisher()
    }
    
    // 2. Start all the process with this function. Merge all publishers in one publishers, and all events in one array of Photo.
    public func getPhotos() {

        Publishers.MergeMany(photoPublishers)                                       // Merge many publishers in a one publisher.
            //.collect()                                                            // We can use 'collect' to emits a single array when the upstream publisher finished.
                                                                                    // But if any publisher finished with an Error, any event (receiveValue) is received.
                                                                                    // If we don't use 'collect' and any publisher is an Error, we don't receive a receiveValue event.
            .sink { completion in
                
                if case .failure(let error) = completion {
                    print("Alberto. Images download error: \(error)")
                }
                
            } receiveValue: { photo in                                              // If we use 'collect', we receive one only event with an array of [AnyPublisher<Photo, URLError>]
                self.photos.append(photo)
            }
            .store(in: &subscribers)
    }
}
