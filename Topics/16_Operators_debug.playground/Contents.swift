import UIKit
import Combine

let imagenesPublisher = ["Alias", "Alien", "BasicInstinct", "Paquito", "Aliens", "AmazingStories", "BoysFromBrazil", "AmericanTail"].publisher

// Operator: handleEvents & breakpoint & print
// -------------------------------------------
// They are very useful to help us to debug code

class ImageLogger: TextOutputStream {   // TextOutputStream is a protocol to show custom info about the event that we received when we have subscription.
    
    func write(_ string: String) {
        print("\(string)")
    }
}

imagenesPublisher
    .tryCompactMap { file in
        
        guard let ruta = Bundle.main.url(forResource: file, withExtension: "jpg") else { return nil }
        return try Data(contentsOf: ruta)
    }
    .handleEvents(receiveOutput: { print("Data: \($0.count)") })              // Like a print, we can access to properties to show. The events would be: receiveOutput, receiveSubscription, etc.
    .breakpointOnError()                                                      // Allow stop to debug the code with error condition on previous operators.
    .breakpoint(receiveOutput: { $0.count == 0 })                             // Stop when no images arrive condition (in this case, possibly when tryCompactMap returns error).
    .compactMap { dataImage in
        UIImage(data: dataImage)
    }
    .print("Received event", to: ImageLogger())                               // Like a handleEvent, but all in strings. Our custom class ImageLogger, manage output info of all event that we received.
    .sink { completion in

        switch completion {
        case .finished:
            print("Download has finished")
        case .failure(let error):
            print("Image download error: \(error)")
        }
    }
    receiveValue: { image in
        print("Received image: \(image)")
    }

