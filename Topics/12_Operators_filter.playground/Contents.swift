import Foundation
import Combine

let number = [1,5,7,9,9,3,5,7,6,4,4,5,12,2,4].publisher

// Operator: filter
// ----------------

number
    .filter { $0.isMultiple(of: 3) }
    .sink {
        print("Multiples of 3: \($0)")
    }

// Operator: removeDuplicates
// --------------------------
// Example of remove duplicates correlatives and doesn't emit event with duplicates.

number
    .removeDuplicates()
    .sink {
        print("Remove correlatives duplicates: \($0)")
    }

// Operator: ignoreOutput
// ----------------------
// Example of remove all events, except the completion.

number
    .ignoreOutput()
    .sink { completion in
        if case .finished = completion {
            print("We finished")
        }
    } receiveValue: { value in
        print("New event, value: \(value)")     // This code is never executed with .ignoreOutput operator.
    }

// Operator: first
// ---------------
// Example of get only the first value of [condition].
// Is needed to have a finish in the publisher.

number
    .first { $0 % 3 == 0 }
    .sink {
        print("First value: \($0)")
    }

// Operator: last
// --------------
// Example of get only the last value of [condition].
// Is needed to have a finish in the publisher.

number
    .last { $0 % 3 == 0 }
    .sink {
        print("Last value: \($0)")
    }

// Operator: drop
// --------------
// We can emit values until another publisher does it with untilOutputFrom.

let semaphore = PassthroughSubject<Void, Never>()
let numbers = PassthroughSubject<Int, Never>()

numbers
    .drop(untilOutputFrom: semaphore)                // When semaphore (publisher) send the first value or event, numbers (another publisher) start to emit events.
    .sink {
        print("Drop value: \($0)")
    }

let nums = [1,5,7,9,9,3,5,7,6,4,4,5,12,2,4]

nums.forEach { n in
    
    if n == 3 {
        semaphore.send()
    }
    numbers.send(n)
}
