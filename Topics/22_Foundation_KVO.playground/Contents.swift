import Foundation
import Combine

final class Test: NSObject {
    
    @objc
    dynamic var anyProperty: Int = 0                  // The property must be dynamic var.
}

let test = Test()

test.publisher(for: \.anyProperty)                    // We can to have a publisher for all objects of NSObject and the properties dynamic var.
    .map {

        print("Receive value: \($0)")
        return $0
    }
    .sink {
        print($0)
    }

test.anyProperty = 1
sleep(1)
test.anyProperty = 2
sleep(1)
test.anyProperty = 3
sleep(1)
test.anyProperty = 4
