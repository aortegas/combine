import Combine

let publisher = [1,4,6,5,7,8,6,5].publisher     // All coleccions are publishers. We get the publisher with .publisher

let subscriber = publisher.sink {   // Get subscriber from publisher.
    print("Recibido \($0)")
}

// Example: We can assign values of a publisher in a object.
class UnObjeto {

    var valor = "" {
        didSet {
            print("Asignado el valor \(valor)")
        }
    }
}

let unObjeto = UnObjeto()

let publisher2 = ["Hola", "Adiós", "Hasta luego", "¿Qué tal?"].publisher
let subscriber2 = publisher2.assign(to: \.valor, on: unObjeto) // We are doing several things here:
                                                               // - assing retuns a subscriber, we are recibing all values of a publisher.
                                                               // - We are setting each value received (with keyPath) on a whatever object.
                                                               // - We become a subscriber of publisher. (other way similar to .sink)
                                                               // In summary, we can receive and assing values of a publisher on any object.
    
print("Ultimo valor guardado en unObjeto: \(unObjeto.valor)")

