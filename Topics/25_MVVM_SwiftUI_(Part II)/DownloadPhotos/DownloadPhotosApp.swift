//
//  DownloadPhotosApp.swift
//

import SwiftUI

@main
struct DownloadPhotosApp: App {
    
    var body: some Scene {
        
        WindowGroup {
            PhotosView()
        }
    }
}
