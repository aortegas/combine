import Foundation
import Combine

extension Notification.Name {
    static let myNotification = Notification.Name("MyNotificacion")
}

final class SenderClass {

    var timer: Timer?

    init() {

        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { _ in
            NotificationCenter.default.post(name: .myNotification, object: "Secret message: \(Int.random(in: 1...50))")
        }
    }
}

final class ReceiverClass {
    
    var message = "" {
        didSet {
            
            if !(message.isEmpty) {
                print("New message: \(message)")
            }
        }
    }

    var subscribers = Set<AnyCancellable>()

    init() {
        
        NotificationCenter.default
            .publisher(for: .myNotification)        // We get the publisher from NotificationCenter.
            .compactMap { $0.object as? String }
            .assign(to: \.message, on: self)        // We get the subscriber from publisher and we never have errors, so we can assign the value in message var.
            .store(in: &subscribers)
    }
}

let sender = SenderClass()
let receiver = ReceiverClass()


