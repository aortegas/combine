import UIKit
import Combine

let imagenesPublisher = ["Alias", "Alien", "BasicInstinct", "Paquito", "Aliens", "AmazingStories", "BoysFromBrazil", "AmericanTail"].publisher

// Operator: tryCompactMap
// -----------------------
// All operator have their try... operator. These operators are very useful to manage try without do / catch blocks.
// tryCompactMap is like a compactMap

imagenesPublisher
    .tryCompactMap { file in
        
        guard let ruta = Bundle.main.url(forResource: file, withExtension: "jpg") else { return nil }
            
        return try Data(contentsOf: ruta)           // This function can throw an exception. This exception will be sent to the next operators.
    }
    .compactMap { dataImage in                      // We use compactMap in this case, because the constructor of UIImage can returns nil.
        UIImage(data: dataImage)
    }
    .sink { completion in

        if case .failure(let error) = completion {  // We must implement completion, because we have a possible exception thrown in the previous operators.
            print("There is an error: \(error)")
        }
    }
    receiveValue: { image in

        print("Image: \(image)")
        image
    }

// Operator: collect
// -----------------
// collect emits a single array when the upstream publisher finished.
// The next example is similar to previous example, but in this case we use collect.
// We use collect without parameters (we receive one event) or we use a value to receive in groups of this values (we receive "n" events).

imagenesPublisher
    .tryCompactMap { file in

        guard let ruta = Bundle.main.url(forResource: file, withExtension: "jpg") else { return nil }
        
        return try Data(contentsOf: ruta)
    }
    .compactMap { dataImage in
        UIImage(data: dataImage)
    }
    .collect(2)
    .sink { completion in

        switch completion {
        case .finished:
            print("Download has finished.")
        case .failure(let error):
            print("Image download error: \(error)")
        }
    }
    receiveValue: { images in

        images.forEach { image in           // In this case, images is an array of images, due we have a collect.
            
            print("Collect: \(image)")
            image
        }
    }

