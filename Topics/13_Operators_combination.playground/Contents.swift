import Foundation
import Combine

let pub1 = [1,2,3].publisher
let pub2 = [4,5,6].publisher

// Operator: append
// ----------------
// In this example: Add publisher 2 to publisher 1, and this send all values. When publisher 1 has finished with its values, send values of publisher 2.

pub1
    .append(pub2)
    .sink { value in
        print("Append Pub2 : \(value)")
    }

// Operator: prepend
// -----------------
// In this example, is the opposite of append. The events from publisher 2 are sent before the values from publisher 1.

pub1
    .prepend(pub2)
    .sink { value in
        print("Prepend Pub2 : \(value)")
    }

// Operator: merge
// ---------------
// Is similar to append, but in this case, send values when each publisher does.

let sub1 = PassthroughSubject<Int, Never>()
let sub2 = PassthroughSubject<Int, Never>()

sub1
    .merge(with: sub2)
    .sink { completion in
        if case .finished = completion {
            print("We have finished both publishers")
        }
    } receiveValue: { value in
        print("Merge Pub1&Pub2 \(value)")
    }

sub1.send(1)
sub1.send(2)
sub2.send(3)
sub1.send(4)
sub2.send(5)
sub1.send(6)
sub1.send(7)
sub1.send(completion: .finished)
sub2.send(8)
sub2.send(completion: .finished)

// Operator: combineLatest
// -----------------------

let sub3 = PassthroughSubject<Int, Never>()
let sub4 = PassthroughSubject<String, Never>()  // Important: Publisher 4 may have other type.

sub4
    .combineLatest(sub3)                        // Combine the last value of publisher 3 with the last values of publisher 4.
    .sink { value in
        print("Combine Latest Sub3 : \(value)")
    }

sub3.send(1)
sub4.send("9")
sub3.send(2)
sub3.send(4)
sub4.send("10")
sub3.send(6)
sub3.send(7)
sub3.send(completion: .finished)
sub4.send("11")
sub4.send(completion: .finished)

// Operator: zip
// -------------
// Is like a merge and combineLastest operators, but in this case, combine in "pairs" of both publishers and publisher event with all different types and its values.

let sub5 = PassthroughSubject<String, Never>()
let sub6 = PassthroughSubject<Int, Never>()

sub5
    .zip(sub6)
    .sink { value in
        print("Zip Sub5&Sub6 : \(value)")
    }

sub5.send("a")
sub5.send("b")
sub6.send(3)
sub5.send("c")
sub6.send(5)

