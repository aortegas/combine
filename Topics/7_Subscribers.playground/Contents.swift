import Combine
import Foundation

class PublisherClass {
    
    // Publisher normally is an array of futures. The Future is a publisher with one use.
    var future: Future<String, Never> {  // Computed Property that returns a Future.

        Future<String,Never> { promise in

            DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
                promise(.success("OK"))
            }
        }
    }
    
    var subscriber: AnyCancellable?
    var subscribers = Set<AnyCancellable>()
    
    func start() {
    
        subscriber = future.sink { value in // Get a subscriber from future.
            print("\(value)")
        }
        
        // Collections are a Publishers.
        [1,3,5,6,7,8,10]    // Get a publisher of array, transform, get a subscriber and store subcriber in set of subscriber.
            .publisher
            .map { "\($0) €" }
            .sink { value in
                print("\(value)")
            }
            .store(in: &subscribers)    // Example for store a subscriber.
    }
}

let publisherClass = PublisherClass()
publisherClass.start()
publisherClass.subscribers.count
